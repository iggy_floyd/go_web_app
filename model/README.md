# model
--
    import "."

@ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

This is a model layer. It defines methods to work with database (here boltDB).


### References

Here is a list of open publications used to prepare this package:

### A User Model

## Usage

#### func  GetMD5Hash

```go
func GetMD5Hash(text string) string
```
a helper to get md5 hashes of passwords...

#### type IUser

```go
type IUser interface {
	UserInfo() []byte
	Save2DB() error
	ReadFromDB() error
	CheckUser() bool
}
```

IUser interface

#### type User

```go
type User struct {
	Email  string
	Passwd string
}
```

a user record

#### func (*User) CheckUser

```go
func (u *User) CheckUser() bool
```
to validate the user: if it exist in the DB or not...

#### func (*User) ReadFromDB

```go
func (u *User) ReadFromDB() error
```
to get a user from the DB http://www.progville.com/go/bolt-embedded-db-golang/

#### func (User) Save2DB

```go
func (u User) Save2DB() error
```
to save a user to the DB http://www.progville.com/go/bolt-embedded-db-golang/

#### func (User) UserInfo

```go
func (u User) UserInfo() []byte
```
to print a user info
