// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
//
// This is a model layer. It defines methods to work with database (here boltDB).
//
// References
//
// Here is a list of open publications used to prepare this package:
//
//
//
package model
