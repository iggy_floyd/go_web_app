# go_web_app
--
@ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de


### Introduction

go_web_app is a simple package illustrating a basic web service realized with
means of the GO.

You can clone the git repo

    git clone https://iggy_floyd@bitbucket.org/iggy_floyd/go_web_app.git

Afterwards, compile and execute the binary

    cd go_web_app
    make

Then you can open documentation in the browser:

    go get code.google.com/p/go.tools/cmd/godoc
    go get -u bitbucket.org/iggy_floyd/virtual_machine_python/go_web_app  #(this repo is made to be public, to allow such "go-getting" )
    godoc -http=:8080
    firefox http://localhost:8080/pkg/bitbucket.org/iggy_floyd/go_web_app

### An_example_of_use

Start the service

    go run server.go

Then open a browser @localhost:8300. The login and password are
`test@example.com` and `test1234`.

### How_to_build

    CGO_ENABLED=0  go build -a


### References

Here is a list of open publications used to prepare this package
