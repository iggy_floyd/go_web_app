package controller

// the model layer is required
import (
   "net/http"    
   "log"
   . "../model"
)


//a handler for the /login
func SignInHandler(w http.ResponseWriter, r *http.Request) {

    if r.Method == "GET" {
     http.Redirect(w, r, "/signin", http.StatusFound)
    } else {
      r.ParseForm()
      if !CheckSession(r) {
        m := make(map[interface{}]interface{})
        m["email"] =   r.Form["email"]
        u := User{r.Form["email"][0],r.Form["password"][0]}
        if u.CheckUser() {
         SetSession(w,r,m)
        }
        log.Println("Session is invalid")
        http.Redirect(w, r, "/", http.StatusFound)
      } else {
        log.Println("Session is valid")
        http.Redirect(w, r, "/", http.StatusFound)
      }
    }
}

//a handler for the /logout
func LogOutHandler(w http.ResponseWriter, r *http.Request) {
    session,_ := GetSession(r)
    if CheckSession(r) {
      delete(session.Values, "email")
      session.Save(r, w)
    }    
    http.Redirect(w, r, "/login", http.StatusFound)
}

