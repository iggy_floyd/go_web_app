# controller
--
    import "."

@ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

This is a controler layer. It defines routes and handlers of the HTTP requests.


### References

Here is a list of open publications used to prepare this package:

## Usage

#### func  CheckSession

```go
func CheckSession(r *http.Request) bool
```
to check if the session is valid

#### func  GetSession

```go
func GetSession(r *http.Request) (session *sessions.Session, err error)
```
to return the session object

#### func  LogOutHandler

```go
func LogOutHandler(w http.ResponseWriter, r *http.Request)
```
a handler for the /logout

#### func  SetSession

```go
func SetSession(w http.ResponseWriter, r *http.Request, m map[interface{}]interface{})
```
to set up a new session

#### func  SignInHandler

```go
func SignInHandler(w http.ResponseWriter, r *http.Request)
```
a handler for the /login
