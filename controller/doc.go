// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
//
// This is a controler layer. It defines routes and handlers of the HTTP requests.
//
// References
//
// Here is a list of open publications used to prepare this package:
//
//
//
package controller
