// @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
//
// This is a view layer. It defines templates and their rendering
//
// References
//
// Here is a list of open publications used to prepare this package:
//
//
//
package view
