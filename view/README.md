# view
--
    import "."

@ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de

This is a view layer. It defines templates and their rendering


### References

Here is a list of open publications used to prepare this package:

## Usage

#### func  RenderTemplate

```go
func RenderTemplate(w http.ResponseWriter, r *http.Request, p *Page)
```
a function to render templates

#### type Page

```go
type Page struct {
	Title   string
	Body    string
	Footer  template.HTML
	Session string
}
```

templates parametes are defined via the Page structure.
