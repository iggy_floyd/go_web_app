package main

//importing the view and controller layers
import ( 
	. "./view"
        . "./controller"
          "log"
          "net/http"
          "html/template"
          "strings"
)

// Page specifies the template parameters
var p *Page


func main() {

  p  = &Page{
		Title: "Test", 
		Body: "Test" ,
		Footer: template.HTML("&copy; 2016 Igor Marfin"),
	}

// to serve the bootstrap js and css files
   fs := http.FileServer(http.Dir("view/static"))
   http.Handle("/static/", http.StripPrefix("/static/", fs))

// a basic handler: use the view layer
   http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
       if !CheckSession(r) && !strings.Contains(r.URL.Path, "signin"){
	 http.Redirect(w, r, "/login", http.StatusFound)
       }
       if CheckSession(r) {
          session,_ := GetSession(r)
          p.Session = session.Values["email"].([]string)[0]
       } else {
          p.Session = ""
       }
       RenderTemplate(w, r,p)    
   })

// the handlers defined by the controller
   http.HandleFunc("/login",SignInHandler)
   http.HandleFunc("/logout",LogOutHandler)

// start of the service
   log.Println("Server is starting...")
   http.ListenAndServe(":8300", nil)

}
